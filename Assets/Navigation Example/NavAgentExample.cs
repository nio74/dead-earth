﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof (NavMeshAgent))]
public class NavAgentExample : MonoBehaviour {

    // Inspector Assigned variable
    public AIWaypointNetwok WaypointNetwok = null;
    public int currentIndex = 0;
    public bool HasPath = false;
    public bool PathPending = false;

    //Private Members
    private NavMeshAgent _navAgent = null;
	void Start () {

        //Cahce NavMeshAgent Reference
        _navAgent = GetComponent<NavMeshAgent>();

        if (WaypointNetwok == null) return;
        SetNextDestination(false);
	
	}

    void SetNextDestination(bool increment)
    {
        if (!WaypointNetwok) 
            return;

        int incStep = increment ? 1 : 0;
        Transform nextWaypointTransform = null;

        while (nextWaypointTransform == null)
        {
            int nextWaypoint = (currentIndex + incStep >= WaypointNetwok.Waypoints.Count) ? 0 : currentIndex + incStep;
            nextWaypointTransform = WaypointNetwok.Waypoints[nextWaypoint];

            if (nextWaypointTransform!=null)
            {
                currentIndex = nextWaypoint;
                _navAgent.destination = nextWaypointTransform.position;
            }
        }

    }
	
	// Update is called once per frame
	void Update () {

        HasPath = _navAgent.hasPath;
        PathPending = _navAgent.pathPending;

        if (!HasPath && !PathPending)
        {
            SetNextDestination(true);
        }
	}
}
